-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `survey`;
CREATE TABLE `survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_add` datetime DEFAULT CURRENT_TIMESTAMP,
  `position` int(11) NOT NULL DEFAULT '0',
  `color_back` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color_font` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `question` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `survey` (`id`, `name`, `date_add`, `position`, `color_back`, `color_font`, `question`) VALUES
(1,	'Otázka roku 2017',	'2017-12-03 21:30:40',	1,	NULL,	NULL,	'Šel bych dnes plavat?'),
(2,	'Nový prezident',	'2017-12-03 21:32:24',	2,	NULL,	NULL,	'Váš tip na prezidenta?'),
(3,	'Víte to?',	'2017-12-04 02:19:06',	3,	NULL,	NULL,	'Míč je:');

DROP TABLE IF EXISTS `survey_answer`;
CREATE TABLE `survey_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) DEFAULT NULL,
  `survey_item_id` int(11) DEFAULT NULL,
  `date_add` datetime DEFAULT CURRENT_TIMESTAMP,
  `answer_string` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_index` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F2D38249B3FE509D` (`survey_id`),
  KEY `IDX_F2D38249564371E5` (`survey_item_id`),
  CONSTRAINT `FK_F2D38249564371E5` FOREIGN KEY (`survey_item_id`) REFERENCES `survey_item` (`id`),
  CONSTRAINT `FK_F2D38249B3FE509D` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `survey_answer` (`id`, `survey_id`, `survey_item_id`, `date_add`, `answer_string`, `user_index`) VALUES
(1,	1,	1,	'2017-12-04 00:46:18',	'Ano',	'123'),
(2,	2,	3,	'2017-12-04 00:47:04',	'Zeman',	'123'),
(3,	1,	1,	'2017-12-04 00:50:34',	'Ano',	'658'),
(4,	2,	5,	'2017-12-04 00:50:40',	'Horáček',	'658'),
(5,	1,	2,	'2017-12-04 00:52:14',	'Ne',	'5760'),
(6,	1,	1,	'2017-12-04 00:52:17',	'Ano',	'40579'),
(7,	2,	4,	'2017-12-04 00:52:20',	'Drahoš',	'69889'),
(8,	1,	1,	'2017-12-04 01:24:34',	'Ano',	'1619'),
(9,	1,	1,	'2017-12-04 01:24:36',	'Ano',	'95665'),
(10,	1,	1,	'2017-12-04 01:24:38',	'Ano',	'52146'),
(11,	1,	1,	'2017-12-04 01:24:39',	'Ano',	'28610'),
(12,	1,	1,	'2017-12-04 01:24:45',	'Ano',	'30554'),
(13,	1,	2,	'2017-12-04 01:26:42',	'Ne',	'79099'),
(14,	1,	2,	'2017-12-04 01:26:44',	'Ne',	'23480'),
(15,	1,	2,	'2017-12-04 01:26:45',	'Ne',	'55071'),
(16,	1,	2,	'2017-12-04 01:26:48',	'Ne',	'53357'),
(17,	1,	2,	'2017-12-04 01:26:49',	'Ne',	'57420'),
(18,	1,	1,	'2017-12-04 01:26:50',	'Ano',	'7686'),
(19,	1,	1,	'2017-12-04 01:26:51',	'Ano',	'4648'),
(20,	1,	1,	'2017-12-04 01:26:52',	'Ano',	'28325'),
(21,	1,	1,	'2017-12-04 01:26:52',	'Ano',	'69519'),
(22,	2,	4,	'2017-12-04 01:26:53',	'Drahoš',	'7945'),
(23,	2,	4,	'2017-12-04 01:26:55',	'Drahoš',	'78917'),
(24,	2,	4,	'2017-12-04 01:26:56',	'Drahoš',	'79016'),
(25,	2,	4,	'2017-12-04 01:26:56',	'Drahoš',	'60424'),
(26,	2,	3,	'2017-12-04 01:26:57',	'Zeman',	'82758'),
(27,	2,	3,	'2017-12-04 01:26:58',	'Zeman',	'89505'),
(28,	2,	3,	'2017-12-04 01:26:59',	'Zeman',	'88809'),
(29,	2,	5,	'2017-12-04 01:27:00',	'Horáček',	'92092'),
(30,	2,	5,	'2017-12-04 01:27:01',	'Horáček',	'67843'),
(31,	2,	5,	'2017-12-04 01:27:02',	'Horáček',	'46349'),
(32,	1,	1,	'2017-12-04 01:27:06',	'Ano',	'77316'),
(33,	1,	1,	'2017-12-04 01:27:08',	'Ano',	'31174'),
(34,	1,	1,	'2017-12-04 01:27:09',	'Ano',	'54576'),
(35,	1,	1,	'2017-12-04 01:27:09',	'Ano',	'85747'),
(36,	1,	1,	'2017-12-04 01:27:10',	'Ano',	'48216'),
(37,	1,	1,	'2017-12-04 01:27:10',	'Ano',	'49715'),
(38,	1,	1,	'2017-12-04 01:27:11',	'Ano',	'50388'),
(39,	1,	1,	'2017-12-04 01:27:11',	'Ano',	'35069'),
(40,	1,	1,	'2017-12-04 01:27:11',	'Ano',	'25360'),
(41,	1,	1,	'2017-12-04 01:27:11',	'Ano',	'65683'),
(42,	1,	1,	'2017-12-04 01:27:12',	'Ano',	'9631'),
(43,	1,	1,	'2017-12-04 01:27:12',	'Ano',	'76199'),
(44,	2,	4,	'2017-12-04 01:27:17',	'Drahoš',	'44900'),
(45,	2,	4,	'2017-12-04 01:27:18',	'Drahoš',	'17002'),
(46,	2,	4,	'2017-12-04 01:27:19',	'Drahoš',	'33093'),
(47,	1,	2,	'2017-12-04 01:41:15',	'Ne',	'd509e40e67e4387b3f1b1d50c91ee2f1'),
(48,	2,	4,	'2017-12-04 01:41:24',	'Drahoš',	'd509e40e67e4387b3f1b1d50c91ee2f1'),
(49,	1,	1,	'2017-12-04 02:00:45',	'Ano',	'50b34634873fa9092715801bb5acff3d'),
(50,	2,	4,	'2017-12-04 02:00:53',	'Drahoš',	'50b34634873fa9092715801bb5acff3d'),
(51,	3,	7,	'2017-12-04 02:22:53',	'Špičatý',	'd509e40e67e4387b3f1b1d50c91ee2f1');

DROP TABLE IF EXISTS `survey_item`;
CREATE TABLE `survey_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `date_add` datetime DEFAULT CURRENT_TIMESTAMP,
  `color_back` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color_font` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correct` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_D9F225D5B3FE509D` (`survey_id`),
  CONSTRAINT `FK_D9F225D5B3FE509D` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `survey_item` (`id`, `survey_id`, `name`, `position`, `date_add`, `color_back`, `color_font`, `correct`) VALUES
(1,	1,	'Ano',	1,	'2017-12-03 21:31:25',	'red',	NULL,	1),
(2,	1,	'Ne',	2,	'2017-12-03 21:31:35',	'blue',	NULL,	0),
(3,	2,	'Zeman',	1,	'2017-12-03 21:32:49',	'red',	NULL,	1),
(4,	2,	'Drahoš',	2,	'2017-12-03 21:33:05',	'blue',	NULL,	0),
(5,	2,	'Horáček',	3,	'2017-12-03 21:34:38',	'green',	NULL,	0),
(6,	3,	'Hranatý',	1,	'2017-12-04 02:19:30',	'yellow',	NULL,	0),
(7,	3,	'Špičatý',	2,	'2017-12-04 02:19:54',	'black',	NULL,	0),
(8,	3,	'Kulatý',	3,	'2017-12-04 02:20:09',	'green',	NULL,	0),
(9,	3,	'Šišatý',	4,	'2017-12-04 02:20:26',	'blue',	NULL,	0);

-- 2017-12-04 01:25:25
