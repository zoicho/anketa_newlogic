<?php
/**
 * Created by PhpStorm.
 * User: zoich
 * Date: 03.12.2017
 * Time: 22:02
 */

namespace App\FrontModule\Components\Survey;


use App\Model\Survey\SurveyAnswer;
use App\Model\Survey\SurveyAnswerAddResult;
use App\Model\Survey\SurveyAnswerFacade;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Http\Request;
use Tracy\Debugger;

class Survey extends Control
{

    private $idSurvey;

    /** @var EntityManager  */
    private $em;

    /** @var \App\Model\Survey\Survey */
    private $survey;
    /**
     * @var SurveyAnswerFacade
     */
    private $surveyAnswerFacade;

    /** @var SurveyAnswerAddResult */
    private $surveyAnswerAddResult;

    public function __construct(
        $idSurvey,
        EntityManager $em,
        SurveyAnswerFacade $surveyAnswerFacade
    )
    {
        parent::__construct();

        $this->idSurvey = $idSurvey;
        $this->em = $em;

        $this->surveyAnswerFacade = $surveyAnswerFacade;
    }

    public function render()
    {

        $this->survey = $survey = $this->em->getRepository(\App\Model\Survey\Survey::class)->find($this->idSurvey);

        $this->template->idSurvey = $this->idSurvey;

        if(!$survey) {
            $this->template->setFile(__DIR__ . '/templates/notFound.latte');
            $this->template->render();
            return;
        }

        $this->template->answerPercentages = $this->getAnswerPercentages();

        $this->template->survey = $survey;
        $this->template->surveyAnswerAddResult = $this->surveyAnswerAddResult;

        $this->template->setFile(__DIR__ . '/templates/default.latte');

        $this->template->render();
    }

    public function handleVote($surveyItemId)
    {
        $this->surveyAnswerAddResult = $this->surveyAnswerFacade->addAnswer((int)$surveyItemId, SurveyAnswerFacade::CHECK_COOKIE_ONLY);
        $this->redrawControl();
    }

    /**
     * @return array
     */
    private function getAnswerPercentages()
    {
        $surveyAnswersCount = 0;
        $surveyItemAnswers = array();

        $answers = $this->em->getRepository(SurveyAnswer::class)->findBy(array('survey' => $this->survey->getId()));

        if($answers) {
            /** @var SurveyAnswer $answer */
            foreach ($answers as $answer) {
                $surveyAnswersCount++;

                $idSurveyItem = $answer->getSurveyItem()->getId();

                if(!isset($surveyItemAnswers[$idSurveyItem])) {
                    $surveyItemAnswers[$idSurveyItem] = 1;
                } else {
                    $surveyItemAnswers[$idSurveyItem]++;
                }
            }
        }

        foreach ($surveyItemAnswers as $surveyItemId => $surveyItemCount ) {
            $surveyItemAnswers[$surveyItemId] = round(($surveyItemCount * 100) / $surveyAnswersCount,2);
        }

        return $surveyItemAnswers;

    }

}