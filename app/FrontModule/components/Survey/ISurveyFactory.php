<?php
/**
 * Created by PhpStorm.
 * User: zoich
 * Date: 03.12.2017
 * Time: 22:07
 */

namespace App\FrontModule\Components\Survey;

interface ISurveyFactory
{
    /**
     * @param $idSurvey
     * @return Survey
     */
    public function create($idSurvey);

}
