<?php
/**
 * Created by PhpStorm.
 * User: zoich
 * Date: 03.12.2017
 * Time: 20:11
 */

namespace App\FrontModule\Presenters;


use App\FrontModule\Components\Survey\ISurveyFactory;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Multiplier;
use Nette\Application\UI\Presenter;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Presenter
{

    /** @var EntityManager */
    protected $em;

    /** @var ISurveyFactory */
    protected $surveyFactory;

    /**
     * @param EntityManager $em
     * @param ISurveyFactory $surveyFactory
     */
    public function injectBaseDependencies(
        EntityManager $em,
        ISurveyFactory $surveyFactory
    )
    {
        $this->em = $em;
        $this->surveyFactory = $surveyFactory;
    }

    protected function createComponentSurvey()
    {
        $multiplier = new Multiplier(function ($idSurvey) {
            $component = $this->surveyFactory->create($idSurvey);
            return $component;
        });
        return $multiplier;
    }

}