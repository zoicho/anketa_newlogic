<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{

	    /* default router */
		/*$router = new RouteList;
		$router[] = new Route('<presenter>/<action>', 'Homepage:default');
		return $router;
        $router = new RouteList;*/

        $router = new RouteList;

        $admin = new RouteList('Admin');
        $admin[] = new Route('admin/<presenter>/<action>[/<id>]', 'Homepage:default');
        $router[] = $admin;

        $front = new RouteList('Front');
        $front[] = new Route('<presenter>/<action>', 'Homepage:default');
        $router[] = $front;

        return $router;

	}
}
