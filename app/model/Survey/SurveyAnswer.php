<?php
/**
 * Created by PhpStorm.
 * User: zoich
 * Date: 03.12.2017
 * Time: 21:03
 */


namespace App\Model\Survey;

use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class SurveyAnswer
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var Survey
     * @ORM\ManyToOne(targetEntity="App\Model\Survey\Survey")
     */
    protected $survey;

    /**
     * @var SurveyItem
     * @ORM\ManyToOne(targetEntity="App\Model\Survey\SurveyItem")
     */
    protected $surveyItem;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true, options={"default": 0})
     */
    protected $dateAdd;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    protected $answerString;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    protected $userIndex;

    public function __construct(Survey $survey, SurveyItem $surveyItem, $answerString, $userIndex)
    {
        $this->dateAdd = new DateTime();
        $this->survey = $survey;
        $this->surveyItem = $surveyItem;
        $this->answerString = $answerString;
        $this->userIndex = $userIndex;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Survey
     */
    public function getSurvey()
    {
        return $this->survey;
    }

    /**
     * @param Survey $survey
     */
    public function setSurvey($survey)
    {
        $this->survey = $survey;
    }

    /**
     * @return SurveyItem
     */
    public function getSurveyItem()
    {
        return $this->surveyItem;
    }

    /**
     * @param SurveyItem $surveyItem
     */
    public function setSurveyItem($surveyItem)
    {
        $this->surveyItem = $surveyItem;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * @param \DateTime $dateAdd
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;
    }

    /**
     * @return string
     */
    public function getAnswerString()
    {
        return $this->answerString;
    }

    /**
     * @param string $answerString
     */
    public function setAnswerString($answerString)
    {
        $this->answerString = $answerString;
    }

    /**
     * @return string
     */
    public function getUserIndex()
    {
        return $this->userIndex;
    }

    /**
     * @param string $userIndex
     */
    public function setUserIndex($userIndex)
    {
        $this->userIndex = $userIndex;
    }



}