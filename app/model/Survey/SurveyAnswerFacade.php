<?php
/**
 * Created by PhpStorm.
 * User: zoich
 * Date: 04.12.2017
 * Time: 0:00
 */

namespace App\Model\Survey;

use Kdyby\Doctrine\EntityManager;
use Nette\Http\Request;
use Nette\Http\Response;

class SurveyAnswerFacade
{

    /** @var EntityManager */
    private $em;
    /**
     * @var Request
     */
    private $request;

    private $cookieKey;
    /**
     * @var Response
     */
    private $response;

    const CHECK_COOKIE_ONLY = 1;
    const CHECK_IP_ONLY = 2;
    const CHECK_IP_USER_AGENT = 3;

    /**
     * @param EntityManager $em
     * @param Request $request
     * @param Response $response
     */
    public function __construct(EntityManager $em, Request $request, Response $response)
    {
        $this->em = $em;
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * @param $idSurveyItem
     * @param $duplicationCheckType
     * @return SurveyAnswerAddResult
     */
    public function addAnswer($idSurveyItem, $duplicationCheckType)
    {
        $addResult = new SurveyAnswerAddResult();
        $idSurveyItem = (int)$idSurveyItem;

        if(!$this->request->getCookie('surveyNewLogic')) {
            $this->cookieKey = uniqid(false, true);
        } else {
            $this->cookieKey = $this->request->getCookie('surveyNewLogic');
        }
        $this->response->setCookie('surveyNewLogic', $this->cookieKey, '100 days');

        if(!$idSurveyItem) {
            $addResult->addError('Answer failed');
        }

        /** @var SurveyItem $surveyItem */
        $surveyItem = $this->em->getRepository(SurveyItem::class)->find($idSurveyItem);

        if(!$surveyItem) {
            $addResult->addError('Failed to load answer');
        }

        $userIndex = $this->getUserIndex($duplicationCheckType);

        $survey = $surveyItem->getSurvey();

        if($this->isAnswerDuplicated($survey, $userIndex)) {
            $addResult->addError('You\'ve already answered');
            return $addResult;
        }

        $surveyAnswer = new SurveyAnswer($survey, $surveyItem, $surveyItem->getName(), $userIndex);

        $this->em->persist($surveyAnswer);

        $this->em->flush();

        $addResult->setSuccess(true);

        return $addResult;

    }

    /**
     * @param Survey $survey
     * @param $userIndex
     * @return boolean
     */
    private function isAnswerDuplicated($survey, $userIndex)
    {

        $resAnswer = $this->em->getRepository(SurveyAnswer::class)->findOneBy(array('survey' => $survey->getId(), 'userIndex' => $userIndex));

        if($resAnswer) {
            return true;
        } else {
            return false;
        }
    }

    private function getUserIndex($duplicationCheckType)
    {
        $indexArray = array();

        switch ($duplicationCheckType) {
            default:
            case 1:
                $indexArray[] = $this->cookieKey;
                break;
            case 2:
                $indexArray[] = $this->request->getRemoteAddress();
                $indexArray[] = $this->request->getRemoteHost();
                break;
            case 3:
                $indexArray[] = $this->request->getHeaders()['user-agent'];
                $indexArray[] = $this->request->getRemoteAddress();
                $indexArray[] = $this->request->getRemoteHost();
                break;
        }

        return md5(implode('|', $indexArray));
    }

}