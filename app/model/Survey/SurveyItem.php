<?php
/**
 * Created by PhpStorm.
 * User: zoich
 * Date: 03.12.2017
 * Time: 21:02
 */

namespace App\Model\Survey;

use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class SurveyItem
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var Survey
     * @ORM\ManyToOne(targetEntity="App\Model\Survey\Survey")
     */
    protected $survey;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    protected $name;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false, options={"default" = 0})
     */
    protected $position;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true, options={"default": 0})
     */
    protected $dateAdd;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $colorBack;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", options={"default" = 0})
     */
    protected $correct;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $colorFont;

    public function __construct()
    {

        $this->dateAdd = new DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Survey
     */
    public function getSurvey()
    {
        return $this->survey;
    }

    /**
     * @param Survey $survey
     */
    public function setSurvey($survey)
    {
        $this->survey = $survey;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * @param \DateTime $dateAdd
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;
    }

    /**
     * @return string
     */
    public function getColorBack()
    {
        return $this->colorBack;
    }

    /**
     * @param string $colorBack
     */
    public function setColorBack($colorBack)
    {
        $this->colorBack = $colorBack;
    }

    /**
     * @return string
     */
    public function getColorFont()
    {
        return $this->colorFont;
    }

    /**
     * @param string $colorFont
     */
    public function setColorFont($colorFont)
    {
        $this->colorFont = $colorFont;
    }

}