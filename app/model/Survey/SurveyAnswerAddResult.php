<?php
/**
 * Created by PhpStorm.
 * User: zoich
 * Date: 04.12.2017
 * Time: 0:05
 */

namespace App\Model\Survey;

class SurveyAnswerAddResult
{

    /** @var array */
    private $errors;

    /** @var boolean */
    private $success;

    public function __construct()
    {

    }

    public function addError($errorText)
    {
        $this->errors[] = $errorText;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }


}