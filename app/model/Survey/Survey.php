<?php
/**
 * Created by PhpStorm.
 * User: zoich
 * Date: 03.12.2017
 * Time: 21:01
 */


namespace App\Model\Survey;

use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class Survey
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $question;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true, options={"default": 0})
     */
    protected $dateAdd;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false, options={"default" = 0})
     */
    protected $position;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $colorBack;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $colorFont;

    /**
     * @var SurveyItem[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Model\Survey\SurveyItem", mappedBy="survey", cascade={"remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $surveyItems;

    public function __construct()
    {

        $this->dateAdd = new DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * @param \DateTime $dateAdd
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;
    }

    /**
     * @return string
     */
    public function getColorBack()
    {
        return $this->colorBack;
    }

    /**
     * @param string $colorBack
     */
    public function setColorBack($colorBack)
    {
        $this->colorBack = $colorBack;
    }

    /**
     * @return string
     */
    public function getColorFont()
    {
        return $this->colorFont;
    }

    /**
     * @param string $colorFont
     */
    public function setColorFont($colorFont)
    {
        $this->colorFont = $colorFont;
    }

    /**
     * @return SurveyItem[]|ArrayCollection
     */
    public function getSurveyItems()
    {
        return $this->surveyItems;
    }

    /**
     * @param SurveyItem[]|ArrayCollection $surveyItems
     */
    public function setSurveyItems($surveyItems)
    {
        $this->surveyItems = $surveyItems;
    }


}