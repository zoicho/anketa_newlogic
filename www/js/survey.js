/**
 * Created by zoich on 03.12.2017.
 */

var survey;

(function($) {

    function SurveyClass()
    {

        var _this = this;
        this.pathname = window.location.pathname;

        this.init = function()
        {

            $(document).on('click','.survey-vote',function() {
                var surveyItemId = $(this).data('id');
                _this.sendVote(surveyItemId);
            });

        };

        this.sendVote = function(surveyItemId)
        {
            console.log('sending vote item id: ' + surveyItemId);

            //add survey id
            var surveyId = 1;
            $.nette.ajax({
                url: _this.pathname + '?do=survey-' + surveyId + '-vote',
                data:  { 'surveyItemId' : surveyItemId},
                type: 'post',
                complete: function(data) {
                    console.log(data);
                }
            });

        }

    }


    $(document).ready(function() {

        survey = new SurveyClass();
        survey.init();
    });

})(jQuery);
